import java.io.File;
import java.io.IOException;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDNonTerminalField;
import org.apache.pdfbox.pdmodel.interactive.form.PDPushButton;

/**
 * This example will take a PDF document and print all the fields from the file.
 * 
 * @author Ben Litchfield
 * 
 * 
 */
public class PrintPDFFields {

	/**
	 * This will print all the fields from the document.
	 * 
	 * @param pdfDocument
	 *            The PDF to get the fields from.
	 * 
	 * @throws IOException
	 *             If there is an error getting the fields.
	 */
	public void printFields(PDDocument pdfDocument) throws IOException {
		PDDocumentCatalog docCatalog = pdfDocument.getDocumentCatalog();
		PDAcroForm acroForm = docCatalog.getAcroForm();
		List<PDField> fields = acroForm.getFields();

		
	//	System.out.println(fields.size() + " top-level fields were found on the form");

		for (PDField field : fields) {
			processField(field, "|--", field.getPartialName());
		}
	}

	protected void processField(PDField field, String sLevel, String sParent) throws IOException {
		String partialName = field.getPartialName();

		if (field instanceof PDNonTerminalField) {
			if (!sParent.equals(field.getPartialName())) {
				if (partialName != null) {
					sParent = sParent + "." + partialName;
				}
			}
			System.out.println(sLevel + sParent);

			for (PDField child : ((PDNonTerminalField) field).getChildren()) {
				processField(child, "|  " + sLevel, sParent);
			}
		} else {
			String fieldValue = field.getValueAsString();
			StringBuilder outputString = new StringBuilder(sLevel);
			outputString.append(sParent);
			if (partialName != null) {
				outputString.append(".").append(partialName);
			}
			outputString.append(" = ").append(fieldValue);
			
			if(field instanceof PDButton && !(field instanceof PDPushButton))
			{
				try {
					PDButton btn_field = (PDButton) field;
					outputString.append(", allowed=").append(btn_field.getOnValues());
				}catch(IllegalStateException e) {
					System.err.println("Could not getOnValues for " + partialName);
				}
				
			}

			String type = field.getClass().getName();
			if(type.startsWith("org.apache.pdfbox.pdmodel.interactive.form."))
				type = type.substring("org.apache.pdfbox.pdmodel.interactive.form.".length());
			outputString.append(",  type=").append(type);
			System.out.println(outputString);
		}
	}

	/**
	 * This will read a PDF file and print out the form elements. <br />
	 * see usage() for commandline
	 * 
	 * @param args
	 *            command line arguments
	 * 
	 * @throws IOException
	 *             If there is an error importing the FDF document.
	 */
	public static void main(String[] args) throws IOException {
		PDDocument pdf = null;
		
		if (args.length < 1) {
			usage();
			return;
		}
		
		int i = 0;
		for(String arg : args )
		{
			try {
				pdf = PDDocument.load(new File(arg));
				PrintPDFFields exporter = new PrintPDFFields();
				exporter.printFields(pdf);
			} finally {
				if (pdf != null) {
					pdf.close();
				}
			}
		}
	}

	/**
	 * This will print out a message telling how to use this example.
	 */
	private static void usage() {
		System.err.println("usage: org.apache.pdfbox.examples.interactive.form.PrintFields <pdf-file> [pdf-file]...");
	}
}
