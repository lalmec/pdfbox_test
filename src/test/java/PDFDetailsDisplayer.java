import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.pdfbox.examples.interactive.form.PrintFields;
import static org.junit.Assert.*;


public class PDFDetailsDisplayer {

	void display(String filename)
	{
		if(filename == null)
			throw new NullPointerException("Null filename");

		if(filename == "")
			throw new IllegalArgumentException("Null-String filename"); 
		
		Path path = Paths.get(filename);
		if(!Files.exists(path))
			throw new IllegalArgumentException("could not locate file: " + filename);

		try {
			PrintPDFFields.main(new String[]{ filename });
		} catch (IOException e) {
			// TODO Auto-generated catch block
			fail(e.getMessage());
		}

	}
}
