import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Stack;
import java.util.UUID;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static org.junit.Assert.*;
import org.apache.commons.logging.Log;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static java.nio.file.StandardCopyOption.*;

import org.apache.pdfbox.pdmodel.interactive.form.*;
import org.apache.pdfbox.examples.interactive.form.SetField;

public abstract class AbstractPDFFields {
	private Path pdf_file;
	private Stack<String> copies = new Stack<>();

	PDFDetailsDisplayer pdf_displayer = new PDFDetailsDisplayer();
	
	abstract String getTestPDFFileName();

	final protected Path loadFile() {
		String testfile = getTestPDFFileName();
		Path ret = null;
		try {
			ret = Paths.get(ClassLoader.getSystemResource(testfile).toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail("Could not load resource: " + testfile);
		}
		if (ret == null) {
			fail("Could not load resource: " + testfile);
		}
		return ret;
	}

	@Before
	final public void load_original() {
		pdf_file = loadFile();
		copies.push(pdf_file.toAbsolutePath().toString());
	}
	
	@After
	final public void remove_copies()
	{
		copies.empty();
	}
	
	@Before
	public void spring_sanity()
	{
		assertNotNull(pdf_displayer);
	}
	
	
	final protected String copyAndReturnTempFilename()
	{
		return copyAndReturnTempFilename(null);
	}
	
	final protected String copyAndReturnTempFilename(String temp_filename)
	{
		UUID uuid = UUID.randomUUID();
		
		String new_filename = String.format("tmp.%s.pdf",uuid);
		if(temp_filename != null)
		{
			new_filename = temp_filename;
		}
		
		Path folder = pdf_file.getParent().toAbsolutePath();
		Path copy_file = FileSystems.getDefault().getPath(folder.toAbsolutePath().toString(),new_filename);
		
		try {
			Files.copy(pdf_file,copy_file,REPLACE_EXISTING);
		} catch (IOException e) {
			fail(e.getMessage());
		}

		String filename_copy = copy_file.toAbsolutePath().toString(); 
		copies.push(filename_copy);

		return filename_copy;
		
	}

	public void print_fields()
	{
		for(String filename : copies) 
		{
			System.out.println("File details file: " + filename);
			pdf_displayer.display(filename);
			System.out.println("\n\n");
		}

	}

}
